#!/bin/bash

echo "-------------samtools version-------------"
samtools --version
echo "-------------bowtie2 version-------------"
bowtie2 --version
echo "-------------bwa version-------------"
bwa
echo "-------------biopython version-------------"
echo -e "import Bio\nprint('Biopython version: %s' % Bio.__version__)" | python
