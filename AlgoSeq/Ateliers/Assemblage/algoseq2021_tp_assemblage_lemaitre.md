# JC2BIM 2021 -- TP assemblage

> Claire Lemaitre (largement copié/inspiré de ceux de Rayan Chikhi, Camille Marchet et Antoine Limasset)
>
> 08 déc. 2021



## Steps

0. Prepare your working environment 
   1. Install the software with conda
   2. Download the sequencing data

1. Assemble your genome
2. Evaluate your assembly
3. Compare it to the state of the art
4. Go to 1.




## Context

You are given 2 sequencing datasets from the same organism:  _V. cholerae_, one with short Illumina reads, the second with long PacBio reads. The goal is to perform an assembly of the _V. cholerae_ genome. It is known that the genome has 2 chromosomes and is of size ~4 Mbp.

The following datasets are provided as subsampled files (to decrease runtime) but can also be fully downloaded by using their identifiers on the Sequence Read Archive with fastq-dump from the SRA toolkit (https://ncbi.github.io/sra-tools/install_config.html) : 

* PacBio sequencing (these are raw noisy “CLR” reads, not “CCS” or high fidelity  reads): ERR1716491.fastq
  * subsampled file : `ERR1716491.subsampled.fasta.gz`

* Illumina sequencing (paired-end, insert size 150 bp): SRR531199.fastq
  * subsampled file : `SRR531199_interleaved_500k.fastq.gz`

**Data download:** [filesender link (180 Mo)](https://filesender.renater.fr/?s=download&token=c7e8f9fd-9840-44a6-8b94-570408800637)



**Installation:** You will use at least the following tools : `quast`, `minimap2`, `miniasm`,`spades`, `seqkit`.

__Q:__ *Install these tools for instance with `conda/mamba`*

__HINT__: You can create a single environment that contains all these tools.



## Practical sketch

Part 1 consists of playing a bit with the datasets to have a rough idea of their characteristics.

The goal of Part 2 is to obtain a quick, initial assembly of the _V. cholerae_ genome using any of these datasets. For this first attempt, if the assembler asks for any parameter, try to guess a reasonable value but do not over-think it. At this point, it does not matter if the assembly is of poor quality.

In Part 3, you will measure the quality of your initial assembly and recognize that it could possibly be improved. 

Finally in Part 4 you submit your assembly statistics to the state of the art:
[https://docs.google.com/spreadsheets/d/1Way4pN6bBDSBsHFvN4sGqRgKlu3PL1oco1eP17rso4o/edit?usp=sharing](https://docs.google.com/spreadsheets/d/1Way4pN6bBDSBsHFvN4sGqRgKlu3PL1oco1eP17rso4o/edit?usp=sharing)



PART 1: Play with datasets
---------------------------

__Q:__ *How many bases are there in the datasets?*

__Q:__ *What is your mean read length?*

__Q:__ *Find the longest read!*

__Q:__ *What is the estimated coverage?*

__HINT__: You can use seqkit (https://bioinf.shenwei.me/seqkit/usage/)



PART 2: Your first assembly!
---------------------------
__Q:__ *Choose a dataset (short or long reads)*

__Q:__ *Assemble with SPAdes for short reads or miniasm for long reads*

#### Short read assembly with SPAdes

Spades is an Illumina assembler designed for prokaryotic and small eukaryotic genomes. It uses multiple sizes of _k_ (k-mer size in the De Bruijn graph) to construct the best possible contigs. It generally takes longer time and memory than other assemblers. 

Spades website: https://github.com/ablab/spades


Spades runtime can be  long. To speed up in this first attempt, you can use a single size of _k_ (with the `-k` option) and skip the correction module (with the `--only-assembler` option).



#### Long read assembly with miniasm

Miniasm is a quick long read assembler, that works in two steps:

    1. Find overlaps (minimap2)
    2. Generate contigs (miniasm)

Minimap2 Website: https://github.com/lh3/minimap2

Miniasm Website: https://github.com/lh3/miniasm

The resulting contigs are just merged erroneous long reads and still contain many sequencing errors.
Produced contigs should be structurally correct, but at the nucleotide level, there are many mismatches and indels. For most applications, the contigs need to be polished. E.g., using the Racon software
or Minipolish (but it can take time to run, we will not do it in this first quick attempt).

**HINT**: miniasm outputs a `.gfa` file, to obtain the contigs in fasta format, here is one possible command line:

```
awk '/^S/{print ">"$2"\n"$3}' assembly.gfa | fold > assembly.fa
```

 



## PART 3: Assembly Evaluation

To evaluate your assembly, you will run  `Quast` on your contigs fasta file (command `quast.py`).

 A (very nice) manual can be found here [http://quast.sourceforge.net/docs/manual.html](http://quast.sourceforge.net/docs/manual.html)

Move into your QUAST output directory and examine the `report.txt` file. You may also take a look at the HTML file.

Now we will compare your contigs with the reference genome (file `vcholerae_h1.fasta`).
With the `-r` option, Quast will align your contigs on your reference genome and estimate their accuracy.


In real life, you will likely not have a reference genome, but if you have a closely related genome, you can use it as a reference and get some nice stats from QUAST such as `genome fraction` (genome coverage).

__Q:__ *How many large/small misassemblies were made by the assembler?*

__Q:__ *What part of your genome is covered by your contigs?*

__Q:__  *How contiguous is your assembly (N50/N75/NGA50/NGA75 metrics)*

__HINT__: If your contigs contain too many errors, Quast will not align them on the reference. In such cases (ie, miniasm unpolished assembly), you can use the `--min-identity` Quast parameter.



### Visualize your assembly graph (optional)

Use the Bandage software to visualize the assembly graph. It is generally the file that ends with `.gfa`. (Load the file and then click on "Draw graph")

__Q:__ *Is your graph well connected?*

__Q:__ *How many disjoint components is there?*

This provides some indications of whether you had sufficient coverage to perform the assembly. It could also mean that the sequencer produced some reads that did not overlap others.

__Q:__  *Does the graph has many branching nodes?*

This is indicative of variants or repetitions that could not have been resolved by the assembler.



## Part 4 Assembly comparison

__Q:__ *Report your assembly on google doc and compare with the results from the other participants.*

[https://docs.google.com/spreadsheets/d/1Way4pN6bBDSBsHFvN4sGqRgKlu3PL1oco1eP17rso4o/edit?usp=sharing](https://docs.google.com/spreadsheets/d/1Way4pN6bBDSBsHFvN4sGqRgKlu3PL1oco1eP17rso4o/edit?usp=sharing)

At this point, you may be tempted to re-run your assembly  :

* with better parameters, 
* with the other read dataset to compare short vs long read assembly,  
* or with another assembler (see below for other assemblers : [Section Supplementary informationl](#supplemenary-information) ).

Now is your time for your brain (and your computer) to shine and to perform the best assembly!



## To wait, during Part 4, a tiny dataset to visualize the effect of repeats on the assembly graph

Assembly tools used in Part 4 can take a while to run, during waiting time, here is a tiny dataset you can play with, using the already known commands of `SPAdes` and `miniasm`. 

Simulated read datasets based on the sars-cov-2 genome (30 Kb) :

* `covid_SR.fa.gz` : simulated short reads (100 bp) on the reference genome of sars-cov-2
* `covid_medium-repeats_SR.fa.gz` : simulated short reads (100 bp) on a modified version of the reference genome of sars-cov-2, with a few repeats (< 1Kb)
* `covid_medium-repeats_LR.fa.gz` : simulated long reads (type PacBio) on the same modified version of the reference genome of sars-cov-2 as previous dataset, ie. with a few repeats (< 1Kb)

Download link : [filesender (3.7 Mo)](https://filesender.renater.fr/?s=download&token=86bf630d-4218-4810-bde3-b8b852e5fbad)

__Q:__ *Assemble the first read dataset with SPAdes. Does the sars-cov-2 contain any large repeat (say > 100) ?*

__Q:__ *Assemble the second read dataset similarly with SPAdes. Visualize with Bandage the effect of a single repeat of 500 bp on the resulting assembly.*

__Q:__ *Check if the problem is resolved with longer reads by assembling the third dataset with miniasm.*





## Supplemenary information

### Available long reads assemblers

1. Miniasm
2. Raven
3. Flye


#### 1. Miniasm

Miniasm is a rather particular long-read assembler as it does not include a consensus step.
The resulting contigs are just merged erroneous long reads and still contains many sequencing errors.
Produced contigs are structurally correct, but at the nucleotide level, there are many, mismatches and indels.
For most applications, the contigs need to be polished. E.g., using the Racon software
or Minipolish.

  Miniasm Work in two steps:
  1. Find overlaps (minimap2)
  2. Generate contigs (miniasm)
  3. Miniasm do not include a polishing step, but you can try Minipolish (https://github.com/rrwick/Minipolish)

Minimap2 Website: https://github.com/lh3/minimap2

Miniasm Website: https://github.com/lh3/miniasm

#### 2. Raven

Raven is overlap graph assembler based on existing components: Minimap (overlap detection) and Racon (polishing) and use them to produce a clean and contiguous assembly.

Raven website: https://github.com/lbcb-sci/raven

__HINT__: You can obtain a .gfa with the  `--graphical-fragment-assembly` option


#### 3. Flye

Flye is a long read assembler based on an original paradigm. It builds a repeat graph that is conceptually similar to  De Bruijn graph but using approximate matches.
Flye is also able to assemble metagenomes.


Flye website: https://github.com/fenderglass/Flye

#### 4. Canu

Canu is one of the reference assemblers for long reads data. It gives good results, however, in the context of this workshop it might take a while to run and
will not be used for this workshop.


 Canu website: https://github.com/marbl/canu

### Available short reads assemblers
1. Spades
2. MEGAHIT
3. Minia

#### 1. Spades

Spades is an Illumina assembler designed for prokaryotic and small eukaryotic genomes. It does an excellent job of assembling bacteria with short-read sequencing, either multi-cell or single-cell data, and also small metagenomes. It uses multiple sizes of _k_ (k-mer size in the De Bruijn graph) to construct the best possible contigs. It generally takes longer time and memory than other assemblers. It can also improve its contigs using long reads.

Spades website: https://github.com/ablab/spades

 __HINT__: If you run the multiple _k_ assembly, you can take a look at the successive contigs files produced from different values of _k_ in the work_dir/K21/final_contigs.fasta,  work_dir/K33/final_contigs.fasta before the whole process is finished.

#### 2. MEGAHIT

 MEGAHIT is an Illumina assembler designed to assemble large metagenomic experiments. It is very conservative and is able to assemble even low coverage regions. It is very fast and memory-efficient despite the fact that it uses several k-mer sizes.

Megahit website: https://github.com/voutcn/megahit

 __HINT__:  Like Spades, you can specify your own k-mer sizes to use with `--k-list  parameter`

#### 3. Minia

 Minia is an Illumina assembler designed to be resource-efficient and able to assemble very large genomes.
 You can run the GATB pipeline that try to remove sequencing errors from the reads (Bloocoo), generate contigs (Minia) and scafold them (BESST)

 __HINT__:  You can also just use minia to generate a contigs set without reads correction or scafolding

 Minia website: https://github.com/GATB/gatb-minia-pipeline


### Other assemblers (long reads)

+ Canu (https://github.com/marbl/canu/commits/master)
+ Redbean (https://github.com/ruanjue/wtdbg2)
+ FALCON (https://github.com/PacificBiosciences/FALCON)
+ SMARTdenovo (https://github.com/ruanjue/smartdenovo)

### Other assemblers (short reads)

+ AByss (https://github.com/bcgsc/abyss)
+ Unicycler (https://github.com/rrwick/Unicycler)
+ Discovardenovo (https://software.broadinstitute.org/software/discovar/blog/)

