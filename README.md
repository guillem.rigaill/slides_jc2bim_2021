# Supports de cours de l'école [JC2BIM](https://www.gdr-bim.cnrs.fr/ecole-jc2bim/). 

[Page google doc](https://docs.google.com/document/d/1o9PCkdLNUHz11z5FUxyvdksM-v4lDU7W8LVPP-BX6zE) contenant le programme, etc.

### Formulaire pour les ateliers du vendredi rattrapés en visio

[Evento](https://evento.renater.fr/survey/results/nwidyldu)

### Enregistrements 

Mercredi matin: Assemblage (Claire Lemaitre)
https://inria.webex.com/recordingservice/sites/inria/recording/a13ba27f3a2c103abfff005056829883/playback, mot de passe 4gKGRKTs

Mercredi matin: Analyse différentielle (Marie-Laure Martin Magniette)
https://inria.webex.com/recordingservice/sites/inria/recording/02d4e9dc3a3a103abdf700505682aeea/playback , mot de passe 9dKTTucX

Mercredi après-midi: Inférence bayésienne (Mahendra Mariadassou) 
https://inria.webex.com/recordingservice/sites/inria/recording/playback/b5b798f23a54103a8dff00505682446b , mot de passe jSizxEE8 

Mercredi après-midi: TP assemblage (Claire Lemaitre, Rayan Chikhi)
https://inria.webex.com/recordingservice/sites/inria/recording/playback/2b51162b3a65103abbf4005056827a81 , mot de passe XcMaPKm6

Jeudi matin: Méthodes sans alignements (Mikaël Salson, Camille Marchet)
https://inria.webex.com/inria/ldr.php?RCID=b3cbb195063983d97f167431ec56fdaa 
mot de passe wXJW7mjb

Jeudi matin: Analyse de réseaux (Stéphane Robin)
https://inria.webex.com/inria/ldr.php?RCID=852bc8cb9f7f7d597245de0c10b4aedf
 mot de passe Pe4vmyii

Jeudi soir: Science Reproductible (Pierre Peterlongo)
https://inria.webex.com/inria/ldr.php?RCID=8d0b1ff82f1bfb3c4b028e8a94f27afb
mot de passe VkJMvcQ2 



## Algorithmique des séquences

#### Cours et TP

* Alignement de séquences et graines: [supports [pdf]](http://guillem.rigaill.pages.mia.inra.fr/slides_jc2bim/AlgoSeq/Cours/algoseq2021_graines_touzet.pdf)  
* Indexation plein texte: [supports [pdf]](http://guillem.rigaill.pages.mia.inra.fr/slides_jc2bim/AlgoSeq/Cours/algoseq2021-indexation-salson.pdf)
* Assemblage de génomes: [supports [pdf]](http://guillem.rigaill.pages.mia.inra.fr/slides_jc2bim/AlgoSeq/Cours/algoseq2021_assemblage_lemaitre.pdf)
* Alignment-free : [supports [pdf]](http://guillem.rigaill.pages.mia.inra.fr/slides_jc2bim/AlgoSeq/Cours/algoseq2021_alignment-free_salson_marchet.pdf)

#### Ateliers

* Alignement de reads avec Bowtie2 [énoncé [pdf]](http://guillem.rigaill.pages.mia.inra.fr/slides_jc2bim/AlgoSeq/Ateliers/Bowtie/algoseq2021-bowtie.pdf) et [Snakefile (pour l'ex. 2)](https://forgemia.inra.fr/guillem.rigaill/slides_jc2bim/-/blob/main/AlgoSeq/Ateliers/Bowtie/Snakefile). Les diapos sur Bowtie2 sont dans le cours sur l'indexation.
* Atelier BLAST [énoncé [pdf]](http://guillem.rigaill.pages.mia.inra.fr/slides_jc2bim/AlgoSeq/Ateliers/BLAST/algoseq2021_blast_touzet.pdf)

## Reproductibilité

#### Cours et TP

* Workflows scientifiques et reproductibilité: [supports [pdf]](http://guillem.rigaill.pages.mia.inra.fr/slides_jc2bim/Reprod/Cours/reprod2021_scientificworkflows_CohenBoulakia.pdf)
* Reproductibilité et bonnes pratiques: [supports [pdf]](http://guillem.rigaill.pages.mia.inra.fr/slides_jc2bim/Reprod/Cours/reprod2021_reproductibilité_Bretaudeau.pdf)

## Statistiques

#### Cours et TP

* Statistiques inférentielles: [supports [html]](http://guillem.rigaill.pages.mia.inra.fr/slides_jc2bim/Stat/Cours/Stat_inferentielle/stats2021_StatsInferentielles_Mariadassou.html)
* Statistiques inférentielles (TP): [instructions d'installation](http://forgemia.inra.fr/guillem.rigaill/slides_jc2bim/-/tree/main/Stat/Cours/Stat_inferentielle_TP)
* Chaînes de Markov et Modèles de Markov: [supports [pdf]](http://guillem.rigaill.pages.mia.inra.fr/slides_jc2bim/Stat/Cours/stats2021_MarkovHMM_Schbath.pdf)
* Statistiques bayésiennes: [supports [pdf]](http://guillem.rigaill.pages.mia.inra.fr/slides_jc2bim/Stat/Cours/Stat_bayesienne/source/bayes/stats2021_bayes_Mariadassou.pdf)
* Analyse différentielle: [supports [pdf]](http://guillem.rigaill.pages.mia.inra.fr/slides_jc2bim/Stat/Cours/stat2021_AnalyseDifferentielle_Martin.pdf)
* Network Analysis: [supports [pdf]](http://guillem.rigaill.pages.mia.inra.fr/slides_jc2bim/Stat/Cours/Stat2021_Network_StephaneRobin.pdf)

#### Ateliers

* Corrélation : [supports [pdf]](http://guillem.rigaill.pages.mia.inra.fr/slides_jc2bim/Stat/Ateliers/Stat2021_AtelierCorrelation_MarieLaureMartinMagniette_GuillemRigaill.pdf)
* Tests multiples : [supports [pdf]](http://guillem.rigaill.pages.mia.inra.fr/slides_jc2bim/Stat/Ateliers/stat2021_atelierTestMultiple_MahendraMariadassouGuillemRigaill.pdf)
* DicoExpress : [supports [pdf]](http://guillem.rigaill.pages.mia.inra.fr/slides_jc2bim/Stat/Ateliers/Stat2021_AtelierDiCoExpress_Martin.pdf)
* Segmentation et détection de ruptures : [supports [pdf]](http://guillem.rigaill.pages.mia.inra.fr/slides_jc2bim/Stat/Ateliers/Stat2021_AtelierSegmentation_GuillemRigaill.pdf)
