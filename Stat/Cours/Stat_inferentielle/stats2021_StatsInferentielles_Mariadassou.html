<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>(JC)2BIM 2021 Research School</title>
    <meta charset="utf-8" />
    <meta name="author" content="Mahendra Mariadassou, INRAE" />
    <script src="libs/header-attrs-2.11/header-attrs.js"></script>
    <link href="libs/remark-css-0.0.1/default.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/custom_jc2bim.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

# (JC)2BIM 2021 Research School
### Mahendra Mariadassou, INRAE
### December 2021, Rennes

---

<script type="text/x-mathjax-config">
MathJax.Hub.Config({
  TeX: {
    Macros: {
      P: "{\\mathcal{P}}",
      N: "{\\mathcal{N}}",
      M: "{\\mathcal{M}}",
      D: "{\\mathcal{D}}",
      LN: "{\\mathcal{LN}}",
      Rbb: "{\\mathbb{R}}",
      Nbb: "{\\mathbb{N}}",
      Sbb: "{\\mathbb{S}}",
      Ycal: "{\\mathcal{Y}}",
      Lcal: "{\\mathcal{L}}", 
      bpi: "{\\boldsymbol{\\pi}}",
      bp: "{\\mathbf{p}}",
      bg: "{\\mathbf{g}}",
      bm: "{\\mathbf{m}}",
      bn: "{\\mathbf{n}}",
      bo: "{\\mathbf{o}}",
      bs: "{\\mathbf{s}}",
      bx: "{\\mathbf{x}}",
      bA: "{\\mathbf{A}}",
      bB: "{\\mathbf{B}}",
      bM: "{\\mathbf{M}}",
      bS: "{\\mathbf{S}}",
      bX: "{\\mathbf{X}}",
      bY: "{\\mathbf{Y}}",
      bZ: "{\\mathbf{Z}}",
      balpha: "{\\boldsymbol{\\alpha}}",
      bbeta: "{\\boldsymbol{\\beta}}",
      bfeta: "{\\boldsymbol{\\eta}}",
      bgamma: "{\\boldsymbol{\\gamma}}",
      bphi: "{\\boldsymbol{\\phi}}",
      btau: "{\\boldsymbol{\\tau}}", 
      btheta: "{\\boldsymbol{\\theta}}",
      bTheta: "{\\boldsymbol{\\Theta}}",
      bmu: "{\\boldsymbol{\\mu}}",
      bSigma: "{\\boldsymbol{\\Sigma}}",
      bOmega: "{\\boldsymbol{\\Omega}}"
    }
  }
});
</script>

---
class: middle, center

## Statistical Inference

.center[(JC)2BIM 2021 Research School]

.center[Mahendra Mariadassou]

.center[[Slides](https://github.com/jchiquet/JC2BIM18/blob/master/source_GR/Inference_Tutorial.pdf) adapted from Guillem Rigaill's original material]



&lt;img src="imgs/LOGO-GDR_BIM_Vecto.png" width="300px" /&gt;&lt;img src="imgs/Empty.png" width="300px" /&gt;&lt;img src="imgs/logo-inrae.png" width="300px" /&gt;

---
class: inverse, middle, center

# Introduction

---
class: inverse, middle

- ## Why inferential statistics

- ## A refresher on probability distributions

- ## Estimating parameters

- ## Confidence intervals

- ## Statistical tests

---

## A short introduction

- **Why** statistical inference in the era of Machine Learning / Artificial Intelligence ? 

  - it helps to understand ML/AI methods
  - the art of making numerical conjectures about puzzling questions
  - statistical reasoning is useful in experimental sciences

--

- Essential for many .alert[data tasks] : manipulation, compression, visualisation, etc
  - Many dataviz are based on **implicit** statistical models  
&lt;img src="imgs/cas_journ.jpeg" width="300px" style="display: block; margin: auto;" /&gt;

--

- Focus on **inferential statistics**
  - (large) class of methods aimed at **generalizing observations** made on a **sample** to a **population**

---

## Studying a population

- One often make .alert[statements] like:
    - *this gene is downregulated in lung cancer*
    - *the cost of Plougastel strawberries rose by 10% compared to last year*
    - *99% of the seeds in these bags are viable*

&lt;br&gt;

- In most of these cases
    - the population we are (implicitly) taking about is **very large**
    - collecting data is time consuming / costly / destructive
    - our measurements are inherently **noisy**
---

## Studying a population (inference)

Hence the data we collect on this population are not "perfect" (and often far from it)
  
  - How can we make statements about the **whole** population ?
  
  
  - We need **assumptions** about the way data were collected
  
  - We need **assumptions** about the **distribution** of the data
  
  - Those assumptions should be **known and explicit** rather than **implicit**
  
  - **Mathemical models** are a tool to spell out those assumptions. 
  
---

## 4 levels of statistical expertise (1)

.pull-left-70[
- **Cookbook**
 
  - If the data is such and such do this and this...
  - Apply the code instructions of an online vignette/tutorial
  - Click on the correct button of a software with a GUI
  - **Requires pattern matching skills**
]

.pull-right-30[
![Cookbook](imgs/brain_1.png)
]

--

.pull-left-70[
- **Applied statistics**
  
  - understand the **underlying statistical models**
  - assess whether a method is valid (or not) to infer a model
  - **Only requires undergrad maths / basic knowledge of R**
]

.pull-right-30[
![Cookbook](imgs/brain_2.png)
]
  
  
---

## 4 levels of statistical expertise (2)


.pull-left-70[
- **Apprentice statistician**
    - Understand mathematical details and algorithmic techniques behind statistical methods
    - **For simple models, requires undergrad maths and basic algorithmic knowledge**
    &lt;br&gt;
    &lt;br&gt;
    &lt;br&gt;
]

.pull-right-30[
![Cookbook](imgs/brain_3.png)
]

--

.pull-left-70[
- **Expert statistician**
  - Develop new methods, new models and associated inference algorithms
  - *Requires advanced mathematical skills and increasingly high algorithmic skills*
]

.pull-right-30[
![Cookbook](imgs/brain_4.png)
]

.footnote[images from [knowyourmeme](https://knowyourmeme.com/memes/galaxy-brain)]

---

class: middle, inverse, center

background-image: url(imgs/lego-assembled-disassembled.jpg)
background-size: cover

# A refresher on probability theory

### .alert[Building bricks for statistical models]

---

## An informal definition

- The **probability space** `\(\Omega\)` is the set of all possible outcomes

- An **event** `\(\omega \subset \Omega\)` is a (well-behaved) subpart of `\(\Omega\)`

- A **probability distribution** `\(p\)` is a map `\(\omega \subset \Omega \mapsto p(\omega) \in [0, 1]\)` such that
  - `\(p(\Omega)=1\)` and `\(p(\emptyset)=0\)`
  - If `\(\omega_1, \omega_2\)` are disjoints (*i.e.* `\(\omega_1 \cap \omega_2 = \emptyset\)`)
  `$$p(\omega_1 \cup \omega_2) = p(\omega_1)+p(\omega_2) \text{ and } p(\omega_1 \cap \omega_2) = 0$$`

---
    
## Examples: a coin flip


.pull-left-70[
- `\(\Omega = \{\text{Head}, \text{Tail}\}\)` 
- `\(p(\text{Head}) = p(\text{Tail}) = 1/2\)`


```r
Omega &lt;- c("Head", "Tail") 
sample(Omega, size = 1, prob = c(1/2, 1/2))
```

```
## [1] "Head"
```

And for **biased** coins (*a.k.a* Bernoulli distributions)
- `\(\Omega = \{\text{Head}, \text{Tail}\}\)` 
- `\(p(\text{Head}) = p\)` and `\(p(\text{Tail}) = 1 - p\)`


```r
p &lt;- 0.2
sample(Omega, size = 10, prob = c(p, 1 - p), 
       replace = TRUE)
```

```
##  [1] "Tail" "Head" "Tail" "Tail" "Tail" "Head" "Tail" "Tail" "Tail" "Tail"
```

]

.pull-right-30[
![](imgs/TwoFaceYearOne.png)
]

.footnote[Art by Jesus Saiz and Jimmy Palmiotti, as he appears in 2008's *Two-Face Year One #2.*]

---

## Examples: dice roll

.pull-left-70[
Fair d4 dice

- `\(\Omega = \{1, 2, 3, 4\}\)` 
- `\(p(\{1\}) = \dots = p(\{4\}) = 1/4\)`


```r
Omega &lt;- c("1", "2", "3", "4") 
sample(Omega, 10, replace=TRUE)
```

```
##  [1] "3" "4" "3" "2" "1" "4" "2" "1" "2" "1"
```

or for **biased** dices (*a.k.a* multinomial distributions)
- `\(p(\{1\}) = p_1, \dots, p(\{4\}) = p_4\)`
- `\(p_1 + \dots + p_4 = 1\)`


```r
prob &lt;- c(1, 2, 3, 4)/10
sample(Omega, 10, prob = prob, replace=TRUE)
```

```
##  [1] "4" "3" "1" "4" "2" "4" "4" "4" "4" "4"
```

]

.pull-right-30[
![](imgs/opaque-blue-d4-top-read__48941.1614210769.jpg)
]

.footnote[image from [dicegamedepot](https://www.dicegamedepot.com/4-sided-opaque-dice-d4-blue/)]

---

## Examples: k-mers in genomes

Count of a random heptamer in a random (bacterial) genome of length 1.65 Mb


```r
## Poisson
rpois(n=1, lambda=60)
```

```
## [1] 73
```

Length between *ori* and the first occurence of a given random heptamer (in a random genome)


```r
## Negative Binomial
rnbinom(n=1, prob = 1/4^7, size = 1)
```

```
## [1] 112399
```

---

## Some useful properties

- For an event `\(A\)` `$$p(\bar{A}) = p(\Omega \setminus A) = 1 - p(A)$$`
- For two events `\(A, B\)`
$$
p(A \cup B) = p(A)+p(B) - p(A \cap B)
$$
.center[
![Inclusion exclusion principle](imgs/inclusion_exclusion.png)
]

.footnote[image from [brillant.org](https://brilliant.org/wiki/principle-of-inclusion-and-exclusion-pie/)]
---

## Independence / conditional probability

- `\(A\)` is .alert[independent] of `\(B\)` if 
`$$p(A \cap B) = p(A)P(B)$$`
  - **Example:** A = `\(\{\text{Head on first coin flip}\}\)`, B = `\(\{\text{Tail on second coin flip}\}\)`

--

- For an event `\(A\)` with `\(p(A) &gt; 0\)` we define the .alert[conditional probability]
`\(p(B|A)\)` as 
$$
p(B|A) = \frac{p(A \cap B)}{p(A)}
$$
  - **Example:** A = `\(\{\text{Even result on a d4 roll}\}\)`, B = `\(\{\text{4 on a d4 roll}\}\)`, then `\(P(B|A) = 1/2\)` and `\(P(A|B) = 1\)`

--

  - **Intuitively**, quantifies the knowledge about `\(B\)` encoded in `\(A\)`
      - If `\(A\)` is independent of `\(B\)` then `\(P(B|A) = P(B)\)`
      - If `\(A\)` is highly informative about `\(B\)` then `\(P(B|A)\)` very different from `\(P(B)\)`
¨  
---
class: middle, center, inverse

# Random Variables

---

## Random Variables

**Definition**

`\(Y\)` is a function from `\(\Omega\)` to a space `\(\Ycal\)` (typically `\(\Rbb\)` or `\(\Nbb\)`) characterized by

$$
p(Y \in S) = p(\{ \omega \in F | Y(\omega) \in S\})
$$
--

**Some examples** 

  - `\(Y\)` a binary variable - a coin flip
  
  - `\(Y\)` an integer smaller than `\(6\)` - a d6 dice roll
  
  - `\(Y\)` a real number - distance of a javelin throw
  
  - `\(Y\)` an integer - expression of a gene in an RNAseq experiment
  
  - ...

---

## Independence and random variables

**Definition**

Two random variables `\(Y_1\)` and `\(Y_2\)` are independent if
for all `\(y_1\)` in `\(\Ycal_1\)` and `\(y_2\)` in `\(\Ycal_2\)` we 
have `$$p(Y_1=y_1 \cap Y_2=y_2)= p(Y_1=y_1) p(Y_2=y_2)$$`

**Examples**
- Roll of two (non) dices
$$
P(\{ \text{Dice}_1 = 1 \} \cap \{ \text{Dice}_2 = 4 \}) = P(\text{Dice}_1 = 1) \times P(\text{Dice}_2 = 4)
$$
- Consecutive throws of a dice

.center[
&lt;img src="imgs/opaque-blue-d4-top-read__48941.1614210769.jpg" width="150px" /&gt;&lt;img src="imgs/Empty.png" width="150px" /&gt;&lt;img src="imgs/opaque-blue-d4-top-read__48941.1614210769.jpg" width="150px" /&gt;
]

---

## Cumulative distribution function

- We call `\(\Ycal\)` the set of values taken by `\(Y\)` (e.g `\(\{0, 1\}\)`, `\(\mathbb{N}\)` for discrete values, `\(\Rbb\)` for continuous values)

- For any `\(y\)` in `\(\Ycal\)` we note `\(p(Y=y)=p(y)\)` or `\(f(y)\)` the density `\(P(Y \in [y, y + dy]) = f(y)dy\)`

- We define the cumulative distribution function as `\(P(Y \leq y)\)`.

$$
`\begin{align}
P(Y \leq y) &amp; = \sum_{y' \leq y} p(y') \quad \text{for discrete variables} \\
P(Y \leq y) &amp; = \int_{y' \leq y} f(y')dy' \quad \text{for continuous variables}
\end{align}`
$$

**Intuition** Very useful to assess the .alert[typicality] of a (range of) values

---

### Discrete example: biased d4 dice

Biased dice with `\(p(i) = i/10\)` for `\(i \in \{1, \dots, 4\}\)`

&lt;img src="stats2021_StatsInferentielles_Mariadassou_files/figure-html/unnamed-chunk-10-1.png" style="display: block; margin: auto;" /&gt;

---

### Continuous example: gaussian `\(\mathcal{N}(\mu, \sigma^2)\)`

- Real valued, (meaning `\(\Ycal = \Rbb\)`) random variable with density `\(f(y)= \frac{1}{\sqrt{2\pi\sigma}} e^{-\frac{(y-\mu)^2}{2\sigma^2}}\)`

&lt;img src="stats2021_StatsInferentielles_Mariadassou_files/figure-html/unnamed-chunk-11-1.png" style="display: block; margin: auto;" /&gt;

---

## Quantiles 

- **Inverse** of the cumulative distribution function 

- The .alert[quantile] `\(q_\alpha\)` of order `\(\alpha\)` is defined (in the simplest case) is defined by 

$$
P(Y \leq q_\alpha) = \alpha
$$

--

**Intuition:** You only have probability `\(\alpha\)` of drawing a value smaller than `\(q_\alpha\)`. 

**Note:** adaptation required for discrete variables (due to cdf not being injective) but same general idea

---

## Graphically 

&lt;img src="stats2021_StatsInferentielles_Mariadassou_files/figure-html/unnamed-chunk-12-1.png" width="1000px" style="display: block; margin: auto;" /&gt;

---

class: center, inverse, middle

## Summary statistics

### **Expectation, Variance and Covariance**

---
## Expectation (a.k.a. mean value)

**Definition**

- For discrete variables with probability `\(p\)`

$$ E(Y) = \underset{y \in \Ycal}{\sum} yp(y) $$

- For continuous variables with a density `\(f\)`
$$ E(Y) = {\underset{y \in \Ycal}{\int}} yf(y)dy $$

Captures the .alert[mean value] of `\(Y\)` (.alert[location] parameter)

--

**Examples**:
- Expectation of a fair d4 dice: `\(\frac{1}{4}(1 + 2 + 3 + 4) = 2.5\)`
- Expectation of a Bernoulli parameter `\(p\)`: `\(p \times 1 + (1-p) \times 0 = p\)`
- Expectation of a Gaussian of parameters `\((\mu, \sigma^2)\)`: `\(\int_{\mathbb{R}} \frac{x}{\sqrt{2\pi}\sigma} e^{-(x-\mu)^2/2\sigma^2}dx = \mu\)`

---
## The expectation is linear

- For two random variables `\(Y_1\)`, `\(Y_2\)`: `$$E(Y_1+Y_2) = E(Y_1) + E(Y_2)$$`
- For a constant `\(c\)` and a random variable `\(Y_1\)`:  `$$E(cY_1) = c E(Y_1)$$`

--

- Law of total expectation: `\(E(Y_2) = E( E(Y2 | Y_1) )\)`
$$
`\begin{align}
E[\text{Human height}] &amp; = E[ E[\text{Human height} | \text{Biological sex}] ] \\
&amp; = E[\text{Female height}] \times P(\text{Female}) + E[\text{Male height}] \times P(\text{Male})
\end{align}`
$$

--

**Note** Useful to .alert[decompose] big **complex** problems (heteregeneous populations) into small **simple** problems (homogeneous populations). 

---
## Variance / Standard Deviation

**Definition** The .alert[variance] of a random variable is defined as:

`$$V(Y) = E( (Y - E(Y))^2) = E(Y^2) - E(Y)^2$$`
or in plain language: "Expectation of squared deviations from the mean".

--

Its .alert[standard deviation] is the square root of the variance 

$$
SD(Y) = \sqrt{V(Y)} = \sqrt{ E( (Y - E(Y))^2) }
$$
--
**Intuition:** Captures the .alert[spread] of `\(Y\)` around its mean value (.alert[dispersion] parameter)

--

**Examples:**

* For a Bernoulli variable `\(Y \sim \mathcal{B}(p)\)`, `\(V(Y) = p - p^2 = p(1-p)\)`
* For a Gaussian variable `\(Y \sim \mathcal{N}(\mu, \sigma^2)\)`, `\(V(Y) = \sigma^2\)`


---
### Graphical intuition 

For gaussian with mean `\(0\)` and variance `\(\sigma^2\)` (or standard deviation `\(\sigma\)`)

&lt;img src="stats2021_StatsInferentielles_Mariadassou_files/figure-html/unnamed-chunk-13-1.png" width="1000px" /&gt;

---
## Variance properties 


**Properties**

  1. For two **independent** random variables `\(Y_1\)` and `\(Y_2\)`: `$$V(Y_1+Y_2) = V(Y_1) + V(Y_2)$$`
  2. For a constant `\(c\)` and a random variable `\(Y_1\)`,  `$$V(cY_1) = c^2 V(Y_1)$$`
&lt;!-- 3. For two random variables: `$$V(Y_2) = E( V(Y2 | Y_1) ) + V( E(Y_2|Y_1) )$$` --&gt;

--

**Note** Expectation and variance are nice for .alert[sums of i.i.d variables].

--

.alert[Variance of the mean:] if `\(Y_1, \dots, Y_n\)` are **independent** with same distributions (*e.g.* results of `\(n\)` independent dice rolls)
$$
`\begin{align}
E\left(\frac{1}{n} \sum_{i=1}^n Y_i \right) &amp; = \frac{1}{n} E(Y_1 + \dots + Y_n) = E(Y_1) \\
V\left(\frac{1}{n} \sum_{i=1}^n Y_i \right) &amp; = \frac{1}{n^2} V(Y_1 + \dots + Y_n) = \frac{1}{n} V(Y_1)
\end{align}`
$$

---
## Covariance

**Definition**

`$$Cov(Y_1, Y_2) = E( (Y_1 - E(Y_1)) (Y_2 - E(Y_2)))$$`

`$$Cov(Y_1, Y_2) = E(Y_1Y_2) - E(Y_1) E(Y_2)$$`

Captures the .alert[linear linkage] of `\(Y_1\)` and `\(Y_2\)` (.alert[association] parameter)

**Note**: If `\(Y_1\)` and `\(Y_2\)` are independent, then `\(Cov(Y_1, Y_2) = 0\)`

---
## Covariance is .alert[bilinear]

- For two random variables `\(Y_1\)`, `\(Y_2\)`: 
`$$Cov(Y_1, Y_2)= Cov(Y_2, Y_1)$$`
- For three random variables `\(Y_1\)`, `\(Y_2\)`, `\(Y_3\)`: 
`$$Cov(Y_1+Y_2, Y_3) = Cov(Y_1, Y_3)+Cov(Y_2, Y_3)$$`
- For a constant `\(c\)` and two random variable `\(Y_1, Y_2\)`: 
`$$Cov(cY_1, Y_2)= c Cov(Y_1, Y_2)$$`

--
    
**Example**:

`\(Y_1\)` result of a fair d4 dice roll and `\(Y_2\)` evenness of that dice roll (namely `\(1\)` for odd number, `\(0\)` for even number):

$$
`\begin{align}
E[Y_1 Y_2] &amp; = \frac{1}{4}(1 + 0 + 3 + 0) = 1 \\
E[Y_1] E[Y_2] &amp; = 2.5 \times 0.5 = 1.25
\end{align}`
$$

---
## Linear correlation

**Definition**

`$$Cor(Y_1, Y_2) = \frac{Cov(Y_1, Y_2)}{\sqrt{V(Y_1)V(Y_2)}}$$`

.alert[Scaled version] of the covariance, always taking values in `\([-1, 1]\)` 

--

**Example**:

`\(Y_1\)` result of a fair d4 dice roll and `\(Y_2\)` evenness of that dice roll (*i.e.* `\(1\)` for odd number, `\(0\)` even number):

$$
Cov(Y_1, Y_2) = -0.25 \quad V(Y_1) = 1.25 \quad V(Y_2) = 0.25 \quad Cor(Y_1, Y_2) = \frac{-0.25}{\sqrt{1.25 \times 0.25}} \simeq -0.45
$$

---
class: middle, inverse, center

background-image: url(imgs/lego-assembled-disassembled.jpg)
background-size: cover

# Estimation

---
class: middle, inverse

## Problem reminder

* ###What can we say about the .alert[whole population] given a .alert[finite sample] ?

* ###We need assumptions = a model

* ###Population `\(\Rightarrow\)` Sample `\(\Rightarrow\)` Data `\(\Rightarrow\)` Model `\(\Rightarrow\)` Inference

---
## Data

Given a sample of size `\(n\)`, `\(n\)` random-variables `\(Y_1, \dots, Y_n\)` give rise to observations `\(y_1, \dots, y_n\)`

**Example:**
- `\(Y_i\)` result of `\(i\)`-th dice roll (.alert[random variable])
- `\(y_i\)` observed value of `\(i\)`-th dice roll (.alert[numeric value])

---
## Model

* Specify the .alert[distribution] of the random variables `\(Y_1, ..., Y_n\)` (not always easy): 

* Usually, assume `\(Y_i\)` are .alert[independent and identically distributed] `\(Y_i \sim p_\theta\)` with distribution `\(p_{\theta}\)`
  - replicates, repetitions of the same experiment

* Often `\(\theta\)` (or some function of it) is the parameter we want to estimate.

--

**Example:**

- For a (biased) d4 dice roll `\(\theta = (p_1, p_2, p_3, p_4)\)` 

- Parameter of interest is probability of rolling a `\(1\)`: `\(p_1\)`: 

- Parameter of interest is unevenness: `\(\max_{1\dots 4} |p_i - 1/4|\)` or `\(\sum_{i=1}^4 |p_i - 1/4|\)` or `\(\sum_{i=1}^4 (p_i - 1/4)^2\)`

---
## Estimator / Estimation

**Intuition:** Information about `\(\theta\)` can be extracted from `\((Y_1, \dots, Y_n)\)`. 

- An .alert[estimator] `\(T\)` is a function of `\(Y_1, ... Y_n\)` providing information about `\(\theta\)` 

- An .alert[estimation] `\(t\)` is its **numerical counterpart** computed from the **observations**

- **Important:** `\(T\)` is a .alert[random variable], `\(t\)` is a .alert[numeric value]

--

**Example**: 

A _natural_ estimator for `\(p_1\)` is 
$$ 
`\begin{align}
T &amp; = \bar{p}_1 = \frac{\sum_i 1_{\{Y_i = 1\}}}{n} \\
t &amp; = \hat{p}_1 = \frac{\sum_i 1_{\{y_i = 1\}}}{n}
\end{align}`
$$


.footnote[With good properties]

---
## What to ask an estimator ? 

**Intuition** the estimator should **hit on target** and have small .alert[mean squared error]

$$
`\begin{equation}
MSE(T) = E[(T - \theta)^2] = (E[T] - \theta)^2 + E[(T- E(T))^2] = \underbrace{Bias^2(T)}_{\text{Systematic error}} + \underbrace{Var(T)}_{\text{Dispersion of the estimator}}
\end{equation}`
$$
--

.pull-left[
&lt;img src="imgs/bias_variance.jpeg" width="450px" style="display: block; margin: auto;" /&gt;
]

--

.pull-right[
Coming back to our previous estimator `\(T\)` of `\(p_1\)`

$$
`\begin{align}
E[T] &amp; = E(1_{\{Y_1 = 1\}}) = P(Y_1 = 1) = p_1 \\
V[T] &amp; = \frac{1}{n}V(1_{\{Y_1 = 1\}}) = \frac{p_1(1 - p_1)}{n} \\
\end{align}`
$$
And thus 

$$
MSE(T) = \frac{p_1(1- p_1)}{n}
$$

]

---

## How do we build estimators ?

- **Natural** estimators for some parameters:
    - Empirical mean for the expectation
    - Empirical frequency for the probability of an event
    - etc

But in general ? 

--

.pull-left[
- Come up with something simple
- Use a .alert[generic method]
    - Maximum likelihood
    - Moment matching method
    - Bayesian inference

- Call your resident statistician
]

--

.pull-right[
&lt;img src="imgs/who-ya-gonna-call-your-statistician.jpg" width="250px" /&gt;
]

---
## A primer on likelihood approaches

The .alert[likelihood] of a sample `\(y_1, ...y_n\)` and of parameters `\(\theta\)`
is a function of `\(\theta\)` defined as 

$$
`\begin{equation}
\theta \mapsto L(y_1, \dots, y_n, \theta) = p_{\theta}(Y_1=y_1, ... , Y_n=y_n) 
\end{equation}`
$$

--

**Intuition:** `\(L(\theta, \dots)\)` is a measure of .alert[goodness of fit] of `\(p_\theta\)` to the observations
  - More likely to observe many `\(1\)`s if `\(p_1\)` is large
  - `\(p_1\)` likely to be large if you observe many `\(1\)`s. 

--

If the `\(Y_i\)` are i.i.d, the log-likelihood is often simpler to manipulate

$$
`\begin{equation}
\mathcal{L}(y_1, \dots, y_n, \theta) = \log L(y_1, \dots, y_n, \theta) = \log \left( \prod_{i=1}^n p_{\theta}(Y_i = y_i) \right) = \sum_{i=1}^n \log p_{\theta}(y_i)
\end{equation}`
$$

---
## Maximum Likelihood Estimation

**Rationale:** Find the value `\(\theta\)` that maximizes `\(\mathcal{L}(\theta)\)` (best fit to the data)

$$
`\begin{equation}
\theta_{MLE} = \arg\max_{\theta \in \Theta} \Lcal(y_1, \dots, y_n, \theta) 
\end{equation}`
$$
1. Express `\(\theta_{MLE}\)` as a function of the observations
$$
\theta_{MLE} = t(y_1, \dots, y_n)
$$
2. Derive the Maximum Likelihood Estimator as 
$$
\hat{\theta}_{MLE} = T(Y_1, \dots, Y_n)
$$

**Rationale:** Generally, the MLE has **nice** statistical properties (low MSE, no bias, ...) 

---
### Example: Bernoulli distributions

Assume `\(Y_i\)` are i.i.d Bernouilli variables with parameter `\(p\)`

- `\(p_{\theta}(Y_i= 0) = 1-p\)`
- `\(p_{\theta}(Y_i= 1) = p\)`
- `\(n_1\)` the number of `\(y_i\)` equal to `\(1\)`
  
`$$L(y_1, ..., y_n, p) = \prod_{i=1}^n p_{\theta}(Y_i=y_i) = p^{n_1}(1-p)^{n-n_1}$$`
taking the log
  
`$$\mathcal{L}(y_1, ..., y_n, p) = (n_1)\log(p) + (n-n_1) \log(1-p)$$`

and maximizing
$$
\arg\max_{p} \mathcal{L}(y_1, ..., y_n, p) = \frac{n_1}{n} = \frac{y_1 + \dots + y_n}{n}
$$
gives the estimator

$$
\hat{p} = \frac{Y_1 + \dots + Y_n}{n}
$$

---
### Example: Gaussian distributions

Assume `\(Y_i\)` are i.i.d Gaussian `\(\mathcal{N}(\mu, \sigma^2)\)` with density `\(f_{\mu,\sigma}(Y_i= y_i) = \frac{1}{\sqrt{2\pi \sigma^2}} e^{\frac{-(y_i-\mu)^2}{2\sigma^2}}\)` and note `\(\theta = (\mu, \sigma)\)`

--

.pull-left[ 
The .alert[likelihood] is given by 
`$$L(y_1, ..., y_n, \theta) = \prod_{i=1}^n \frac{1}{\sqrt{2\pi \sigma^2}} e^{\frac{-(y_i-\mu)^2}{2\sigma^2}}$$`
]

.pull-right[
And the .alert[log-likelihood] by 
`$$\mathcal{L}(\theta) = -\frac{n}{2}\log(2\pi) - \frac{n}{2}\log(\sigma^2) - \frac{1}{2\sigma^2} \sum_i (y_i-\mu)^2$$`
]

--

Maximizing in `\(\theta\)` and replacing `\(y_i\)` with `\(Y_i\)` to obtain .alert[estimators] gives the empirical mean and emprical variance

$$
`\begin{align}
\hat{\mu} &amp; = \frac{\sum_i Y_i}{n} = \bar{Y} \\
\hat{\sigma}^2 &amp; = \frac{\sum_i (Y_i - \hat{\mu})^2}{n} = \frac{1}{n} \sum_i (Y_i - \bar{Y})^2 \\
\end{align}`
$$

---
## Numerical illustration


```r
set.seed(42)
## Draw 20 independent bernoulli variables with parameter 0.5
Y &lt;- rbinom(n=20, size=1, prob=0.5) 
## Numerical solution
n1 &lt;- sum(Y)
loglik &lt;- function(p) {
  -dbinom(n1, size = 20, prob = p, log = TRUE)
}
optim(par = 0.5, fn = loglik, method = "L-BFGS-B", lower = 1e-7, upper = 1-1e-7)$par
```

```
## [1] 0.6499999
```

```r
## Analytic solution
p_hat &lt;- mean(Y)
p_hat
```

```
## [1] 0.65
```

---
## Visually for `\(n\)` i.i.d Bernoulli r.v.

.pull-left-70[
&lt;img src="stats2021_StatsInferentielles_Mariadassou_files/figure-html/unnamed-chunk-17-1.png" width="700px" /&gt;
]

.pull-right-30[
&lt;img src="imgs/my-precious.jpg" width="200px" style="display: block; margin: auto;" /&gt;

&lt;br&gt;&lt;br&gt;&lt;br&gt;&lt;br&gt;&lt;br&gt;&lt;br&gt;&lt;br&gt;
&lt;img src="imgs/let-it-go.jpg" width="200px" style="display: block; margin: auto;" /&gt;
]

---
class: inverse, middle

## Small recap 

- ### Sample `\(\rightarrow\)` Data `\(\rightarrow\)` Model `\(\rightarrow\)` Inference

- ### Estimator = Random variable

- ### Estimation = Function of the observations

- ### Likelihood = Goodness of fit measure, used to build estimators automatically

- ### Ideally, an estimator should hit *on target*

---
class: middle, inverse, center

background-image: url(imgs/lego-assembled-disassembled.jpg)
background-size: cover

# Confidence intervals

---

## Motivation 

Given an estimator `\(T\)` of `\(\theta\)`, we can derive:

- a .alert[point estimate] `\(t\)`
- a global measure of quality of `\(T\)`: its .alert[MSE]

--

.center[
#### Can we do better ?
]

--

In many cases, we can go further and give two bounds `\(B_1\)` and `\(B_2\)` such that the true value is .alert[likely] to be in `\([B_1, B_2]\)`. This is called the .alert[confidence interval]. 

**Note:** `\(B_1\)` and `\(B_2\)` are .alert[random values] depending on `\(Y_1, \dots, Y_n\)`.

---

## Definition

**Random interval** 

Let `\(B_1 = m(Y_1, ..., Y_n)\)` et  `\(B_2 = M(Y_1, ..., Y_n)\)` two r.v. We define a .alert[random interval] `\([B_1, B_2]\)` for `\(\theta\)` with the couple `\((B_1, B_2)\)` and call `\(P(B_1 &lt; \theta &lt; B_2)\)` the .alert[level of confidence].

--

**Confidence interval**

A .alert[confidence interval] at level `\(1-\alpha\)` for `\(\theta\)` is a realisation `\([b_1, b_2]\)` of a random interval with confidence level `\(1-\alpha\)`

--

**Notes:** 

- In general, we want the interval to be as .alert[narrow] as possible. 
- `\(\theta\)` is a fixed value and `\([B_1, B_2]\)` is random. For any realisation `\([b_1, b_2]\)` of `\([B_1, B_2]\)`, `\(\theta\)` is .alert[either] in the interval or not. 

---
## Example: gaussian with mean variance

- **Model** 
`\(Y_1, ..., Y_n\)` gaussian with parameter `\(\mu\)` (unknown) and `\(\sigma^2\)` (known)

- **Estimator** 
`$$\bar{Y} = \sum Y_i /n$$`

- We assume for simplicity that `\(V(Y_i) = \sigma^2\)` is known

- Model for the estimator [using properties of gaussian / Central Limit Theorem]

$$
\bar{Y} \sim \mathcal{N}(\mu, \sigma^2/n)
$$

Or equivalently&lt;sup&gt;1&lt;/sup&gt; 

$$
Z = \frac{\bar{Y} - \mu}{\sigma/\sqrt{n}} \sim\mathcal{N}(0, 1)
$$
--
We can use .alert[quantiles] of the standard gaussian to find bounds. 

.footnote[&lt;sup&gt;1&lt;/sup&gt; Using the property `\\(Z \sim \mathcal{N}(0, 1) \Leftrightarrow \mu + \sigma Z \sim \mathcal{N}(\mu, \sigma^2) \\)`.]

---
## Confidence Interval

.pull-left[
With probability 0.95

$$
`\begin{equation}
q_{0.025} \leq \frac{\bar{Y} - \mu}{\sigma/\sqrt{n}} \leq q_{0.975}
\end{equation}`
$$
Or with a bit of manipulation

$$
`\begin{align}
\mu &amp; \in \left[ \bar{Y} - q_{0.975}\frac{\sigma}{\sqrt{n}}, \bar{Y} - q_{0.025}\frac{\sigma}{\sqrt{n}} \right] \\
\mu &amp; \in \bar{Y} \pm 1.96 \frac{\sigma}{\sqrt{n}}
\end{align}`
$$
]

.pull-right[
&lt;img src="stats2021_StatsInferentielles_Mariadassou_files/figure-html/unnamed-chunk-20-1.png" width="500px" /&gt;
]

---
## A cautionary tale

- .alert[On average] 95% of the intervals contain the true value. 
- But for a .alert[single] interval, the true value is .alert[either] in the interval or not in the interval

&lt;img src="stats2021_StatsInferentielles_Mariadassou_files/figure-html/unnamed-chunk-21-1.png" width="1000px" /&gt;

---
class: middle, inverse, center

background-image: url(imgs/lego-assembled-disassembled.jpg)
background-size: cover

# Hypothesis testing

---

## Main Idea

Some research directions can be formulated as .alert[questions] with a yes / no answer: 

- Is the expression of gene HER2 large in breast cancer ?
- Is a new variety of tomato more resistant to mildew than the previous one ?
- Are mRNA vaccines better than placebo ?
- Does it really rain more in Brittany than in the rest of France ?

--

You collect .alert[noisy data] about the problem and want to give an *evidence based* .alert[answer] to the question

--

&lt;img src="imgs/what-if-i-told-you-about-statistical-tests.jpg" width="300px" style="display: block; margin: auto;" /&gt;

---

## When should you use a test ?

* You have a .alert[yes/no] question

* You have .alert[data] related to the question

* The data can be .alert[modeled] as the result of some .alert[random variable]

* The question can be framed in terms of .alert[parameters] of the distribution

--

.pull-left[
You can either:
- **reject** the null hypothesis: observed patterns are unexpected under your assumptions
- **retain** the null hypothesis: observed patterns are expected under your assumptions
  - .red[**Does not imply that your assumptions are true**]
]

--

.pull-right[
&lt;img src="imgs/one-does-not-simply-accept-the-null-hypothesis.jpg" width="300px" style="display: block; margin: auto;" /&gt;
]

---
## Four elements of a test


.pull-left-70[
- **Data:** 
    * `\(y_1, ..., y_n\)` realisation of r.v. `\(Y_1, ..., Y_n\)`
- A statistical **model**: 

    * distribution of  `\(Y_1,..., Y_n\)` depending on some parameters `\(\theta\)`

- An **assumption**: 

    * A statement about `\(\theta\)`. 
    * This is the so called `\(H_0\)` hypothesis that you usually want to reject, `\(H_1\)` being the alternative

- A **decision rule** 

  	* If `\(T=f(X_1,..., X_n)\)` is a test statistic
  	* `\(R\)`  is subset of values for `\(T\)` that is unlikely if `\(H_0\)` is true
]

---
## An example with babies

- **Data**: biological sex of children born in 2020 in France (from [INED and INSEE](https://www.ined.fr/fr/tout-savoir-population/chiffres/france/naissance-fecondite/naissances-sexe/))
  - 356 389 👦 / 340 275 👧

- **Model**: 
  - The sex of a newborn is `\(\sim \mathcal{B}(p)\)`, a Bernoulli of parameter `\(p\)` (where 1 stands for girls and 0 for boys)
  - Sex of all children are i.i.d. (.alert[modeling assumption])
  
- **Null Hypothesis**
  - There is an equal chance of being born a boy or a girl `\(\Rightarrow p = 1/2\)`
  
- **Decision rule**
  - Test statistic: .alert[frequency] `\(\hat{p}\)` of girls among children born in 2020
  - Reject `\(H_0\)` if `\(\hat{p}\)` is **too far** from `\(1/2\)` 
  
--

.center[.alert[The hard work consists in quantifying what is *too far*]]

---
## Decision errors

The decision is based on .alert[noisy data] and so is .alert[error-prone]

&lt;img src="imgs/typeIandII.jpg" width="700px" style="display: block; margin: auto;" /&gt;

.footnote[Image from [whatilearned](https://whatilearned.fandom.com/wiki/Hypothesis_Testing)]

---
### Type I/ Type II error for pregnancy tests

&lt;img src="imgs/Type-I-and-II-errors1-625x468.jpg" width="700px" style="display: block; margin: auto;" /&gt;

---

### A few remarks

- Strong .alert[asymetry] between `\(H_0\)` and `\(H_1\)`

- You want to accumulate sufficient evidence .alert[against] `\(H_0\)`. 

- Analogy with trials: `\(H_0\)` is on trial, do you have enough proof to convict it? 

.center[
**Necessary trade-off** between type I and type II errors:
]
--

.pull-left-70[
- .alert[more stringent] criteria : 
  - less wrongful convictions (type I errors) 
  - more criminals released (type II errors, lack of power)
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
]

.pull-right-30[
&lt;img src="imgs/im-on-steroids-really-prove-it.jpg" width="150px" /&gt;
]

--

.pull-left-70[
- .alert[less stringent] criteria: 
  - less criminals released (type II errors)
  - more wrongful convictions (type I error) 
]

.pull-right-30[
&lt;img src="imgs/sakazuki_guilty.jpeg" width="150px" /&gt;
]

---

## Construction of a test

.pull-left-70[
- Start with an .alert[estimator] for the parameter of intercept:
    - Frequencies `\(F\)` of girls among newborn
]

--

.pull-left-70[    
- Derive the .alert[distribution] of `\(F\)` under `\(H_0\)`
    - For `\(n = 356 389 + 340 275\)`, using the CLT, we have 
    `$$F \sim \mathcal{N}(1/2, 1/4n) = \mathcal{N}(0.5, 3.6 \times 10^{-7})$$`
]

.pull-right-30[
&lt;img src="stats2021_StatsInferentielles_Mariadassou_files/figure-html/unnamed-chunk-28-1.png" width="700px" /&gt;
]

--

.pull-left-70[
- Calibrate the .alert[region of unlikely values] under the null
    - With probability 0.95, `\(|F - 1/2| \leq 1.96 \times \sqrt{3.6\times 10^{-7}} = 0.0012\)`
    - `\(F \notin [0.4988, 0.5012]\)` with probability at most 5%
]

--

.pull-left-70[
- Compare with the .alert[actual estimation]
    - Here `\(f = 340 275 / (356 389 + 340 275) = 0.488\)`
    - Unlikely if `\(p= 1/2\)` `\(\Rightarrow\)` reject `\(H_0\)`
    
]

---

## About the p-value

**Idea** Go one step further than **reject / retain** and assess the .alert[significance] of the observed statistic

--

.pull-left[
- `\(|F - 1/2|\)` unlikely to be higher than `\(0.0012\)`

- `\(|f - 1/2|\)` actually observed to be `\(0.012\)`

- How likely are we to observe such deviations if `\(p=1/2\)`? 

$$
`\begin{align}
p &amp;= P_{H_0}(|F - 1/2| &gt; |f - 1//2|) \\
&amp; = P_{H_0}(|F - 1/2| &gt; 0.012) \\
&amp; = P_{H_0}(F &lt; 0.488) + P_{H_0}(F &gt; 0.512) \\
&amp; \simeq 5.5 \times 10^{-89}
\end{align}`
$$
- Not only unlikely but .alert[insanely] unlikely
]

.pull-right[
&lt;img src="stats2021_StatsInferentielles_Mariadassou_files/figure-html/unnamed-chunk-29-1.png" width="700px" /&gt;
]

---
### A less extreme example

The number of men / women aged 0-19 years in the Rennes Urban area (data from [INSEE](https://www.insee.fr/fr/statistiques/2011101?geo=COM-35238#chiffre-cle-1)): 

24 982 👦 / 25 556 👧


.pull-left[
Repeating the process, we find 

- `\(F \sim \mathcal{N}(0, 4.94\times 10^{-6})\)`

- `\(|F - 1/2|\)` unlikely to be higher than `\(0.45\)`%

- `\(|f - 1/2|\)` actually observed to be `\(0.57\)`%

- How extreme is that deviation ? 

$$
`\begin{align}
p &amp;= P_{H_0}(|F - 1/2| &gt; |f - 1//2|) \\
&amp; = P_{H_0}(|F - 1/2| &gt; 0.057) \\
&amp; = P_{H_0}(F &lt; 0.494) + P_{H_0}(F &gt; 0.506) \\
&amp; \simeq 1.06 \times 10^{-2}
\end{align}`
$$
- Not likely but still a 1% chance to occur
]

.pull-right[
&lt;img src="stats2021_StatsInferentielles_Mariadassou_files/figure-html/unnamed-chunk-30-1.png" width="700px" /&gt;
]

---

class: middle, inverse

## Summary

- #### Collect .alert[data] from a .alert[sample]

- #### Build a .alert[model] for your .alert[observations] with .alert[reasonable] assumptions

- #### State the .alert[question] (hypothesis) in terms of model .alert[parameter]

- #### Build a (good) .alert[estimator] for the parameter of interest

- #### Assess the .alert[uncertainty] of the estimator using .alert[confidence interval]

- #### .alert[Reject or retain] your research hypothesis based on the .alert[level] you want to achieve

- #### Assess the .alert[significance] of your result using .alert[p-values]

&lt;!-- # Multiple testing (a quick introduction) --&gt;
&lt;!-- ```{r child = 'chapter6.Rmd'} --&gt;
&lt;!-- ``` --&gt;
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="libs/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"ratio": "16:9",
"slideNumberFormat": "%current%",
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
